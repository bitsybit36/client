import { getters as g } from './constants'
import db from './dexie'

export default {
    [g.GET_LINES_BY_POINT_ID]: () => async (id) => {
        // console.debug('Get lines for point : %O', id)
        // console.debug('This does not makes use of direction_fk')
        const ids = new Set()
        await db.db.linepoints
            .where('point_fk').equals(id).each(p => {
                ids.add(p.line_fk)
            })
        return Array.from(ids)
    },

    [g.GET_LINE_BY_ID]: () => async (id) => {
        // noinspection JSUnresolvedFunction
        return await db.db.lines.where('id').equals(parseInt(id, 10)).first()
    },

    [g.GET_POINTS_BY_NAME]: () => async (name) => {
        // noinspection JSUnresolvedFunction
        return await db.db.points.where('name_fr').equalsIgnoreCase(name).or('name_nl').equalsIgnoreCase(name).toArray()
    },
    [g.GET_POINTS_BY_LINE]: () => async (lines) => {
        console.debug('Get points for lines : %O', lines)
        const ids = new Set()
        await db.db.positions
            .where('line_fk').anyOf(lines).each(position => {
                ids.add(position.point_fk)
                ids.add(position.direction_fk)
            })

        return await db.db.points.where('id').anyOf(ids).toArray()
    },

    [g.GET_POINT_BY_ID]: () => async (id) => db.db.points.where('id').equals(id).first(),

    [g.GET_NEXT_POINT]: (state, getters) => async ({ line_fk, point_fk, direction_name_fr }) => { // eslint-disable-line camelcase
        try {
            // console.debug('[g.GET_NEXT_POINT] line_fk: %i, point_fk: %i, direction_name_fr: %s', line_fk, point_fk, direction_name_fr)

            const linePoint = await db.db.linepoints
                .where('[line_fk+point_fk+direction_name_fr]').equals([line_fk, point_fk, direction_name_fr]) // eslint-disable-line camelcase
                .first()

            if (!linePoint) {
                console.warn('No order found for station %i on line %i direction %s', point_fk, line_fk, direction_name_fr)
                return null
            }

            const arrivalPoint = await db.db.linepoints
                .where('[line_fk+order+direction_name_fr]').equals([line_fk, linePoint.order + 1, direction_name_fr]) // eslint-disable-line camelcase
                .first()

            if (!arrivalPoint) {
                console.warn('No next point seems to be available...')
                return null
            }

            // console.log('[g.GET_NEXT_POINT] arrivalPoint : %O', arrivalPoint)

            const nextPoint = await getters[g.GET_POINT_BY_ID](arrivalPoint.point_fk)

            // console.debug(line_fk, direction_fk, departurePointName, pointIndex, arrivalPointName, nextPoint)
            return nextPoint
        } catch (e) {
            console.error(e)
            console.info('Next point not found on line %i point %i towards %s', line_fk, point_fk, direction_name_fr)
        }
    },

}
