import moment from '../lib/moment-range'

export default {
    displayedDatetime: moment().valueOf(),
    isRealTime: true,

    allowedDates: [],

    lineStatistics: {},
    pointStatistics: {},

    geojsonSources: {},

    arePointsLoading: false,
    arePositionsLoading: false,

    hydrated: false,

    fetchedRange: null,
}
