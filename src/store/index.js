import db from './dexie'
import { createFlashStore } from 'vuex-flash/src'
import defaultState from './defaultState'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

export default {
    state: defaultState,

    getters,

    actions,

    modules: db.modules,

    plugins: [
        createFlashStore(),
        db.plugin,
    ],

    mutations,
}
