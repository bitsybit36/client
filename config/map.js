import styles from './styles'
const { colors } = styles

const simpleLinePaintProps = {
    'line-width': 8,
    'line-opacity': 0.5,
    // 'line-cap': 'round',
}

export default {
    mapboxToken: process.env.VUE_APP_MAPBOX_TOKEN,
    style: 'mapbox://styles/metrocheck/ckcjikj9u41yb1ipho704geqg',
    maxBounds: [[4.2282982952, 50.7616088978], [4.4857903607, 50.915327968]],
    layers: {
        positionsLayer: {
            id: 'positions',
            type: 'circle',
            paint: {
                'circle-color': [
                    'case',
                    ['==', ['get', 'line_fk'], 1],
                    colors.purple,
                    ['==', ['get', 'line_fk'], 2],
                    colors.orange,
                    ['==', ['get', 'line_fk'], 5],
                    colors.yellow,
                    ['==', ['get', 'line_fk'], 6],
                    colors.blue,
                    '#000000',
                ],
                // styles.colors.orange,
                'circle-stroke-color': '#000000',
                // 'circle-stroke-width': 2,
                'circle-stroke-width': [
                    'match',
                    ['get', 'distance_from_point'],
                    0,
                    1.5,
                    0.5,
                ],
                'circle-stroke-opacity': 0.8,
            },
        },
        // WIP : Try with symbol in order to display text & stuff.
        // positionsLayer: {
        //     id: 'positions',
        //     type: 'symbol',
        //     layout: {
        //         'icon-image': 'bus-11',
        //         'icon-allow-overlap': true,
        //         'text-field': [
        //             'format',
        //             ['upcase', ['get', 'direction_fk']],
        //             { 'font-scale': 0.8 },
        //         ],
        //         'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
        //         'text-offset': [0, 0.6],
        //         'text-anchor': 'top',
        //     },
        // },
        pointsLayer: {
            id: 'points',
            type: 'circle',
            paint: {
                'circle-color': '#ffffff',
                'circle-opacity': 0.6,
                'circle-stroke-color': '#1B1A2B',
                'circle-stroke-opacity': 1,
                'circle-stroke-width': 1,
                'circle-blur': 0.2,
            },
        },
        technicalLayer: {
            id: 'technical',
            type: 'line',
            paint: {
                'line-color': '#333333',
            },
        },
        defaultLineLayer: {
            type: 'line',
        },
    },
    paints: {
        simpleLine: {
            filled: simpleLinePaintProps,
            dashed: {
                ...simpleLinePaintProps,
                'line-dasharray': [1, 1],
            },
        },
    },
}
