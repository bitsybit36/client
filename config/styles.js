const colors = {
    orange: '#F57000',
    blue: '#0078AD',
    yellow: '#E6B012',
    purple: '#C4008F',
}

const styles = {
    colors,
}

export default styles
