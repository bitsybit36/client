const WorkerPlugin = require('worker-plugin')

module.exports = {
    transpileDependencies: [
        'vuetify',
    ],
    devServer: {
        hot: false,
        liveReload: false,
    },
    configureWebpack: {
        output: {
          globalObject: 'this',
        },
        plugins: [
            new WorkerPlugin({
                globalObject: 'this',
            }),
        ],
    },
}
